# Spring Batch Demo

#### Run this project by this command :
1. `git clone https://gitlab.com/hendisantika/spring-batch-demo.git`
2. `cd spring-batch-demo`
3. `mvn clean spring-boot:run`
4. Start Batch --> `GET /batch/start`
5. Stop Batch --> `GET /batch/stop`
6. Continue Batch --> `GET /batch/continue`
7. Rollback Batch --> `GET /batch/rollback`

#### Screen shot

Show Tables

![Show Tables](img/tables.png "Show Tables")

Show credentials tables before

![Show credentials tables](img/credentials.png "Show credentials tables")

Show credentials backup table

![Show credentials backup table](img/backup.png "Show credentials backup table")

Show credentials tables after

![Show credentials tables](img/after.png "Show credentials tables after")
