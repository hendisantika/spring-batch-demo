package com.hendisantika.springbatchdemo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-batch-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-15
 * Time: 14:32
 */
@Data
@AllArgsConstructor
public class Credentials {

    private final String id;
    private final String password;
}
